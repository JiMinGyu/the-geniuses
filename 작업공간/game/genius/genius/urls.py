from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'genius.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'index$', 'game.views.index'),
    url(r'^$', 'game.views.index'),
    url(r'^game_introduction$', 'game.views.introduction'),
    url(r'^room$', 'game.views.room_list'),
    url(r'^room/(?P<name>[a-zA-Z0-9]+)/$', 'game.views.room'),
)
