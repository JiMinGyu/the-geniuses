from django.utils import timezone

from game.models import Room, Player

class AutoLogout:
  def process_request(self, request):
    
    rooms = Room.objects.all()
    
    for room in rooms:
      for room_player in room.join_players.all().order_by('last_approach'):
          if timezone.now() - room_player.last_approach > timezone.timedelta(minutes = 1):#1분 이내 접속한 사람이 아닐 경우
              if room.current_player_number > 0:
                    room.current_player_number -= 1
                    room_player.delete()
              room.is_playing = False
          else:
              break
          if room.current_player_number == 0:
                room.delete()
          else:#room.current_player_number != 0
                room.save()

    players = Player.objects.all().order_by('last_approach')

    for player in players:
        if  timezone.now() - player.last_approach > timezone.timedelta(hours = 1):
            player.delete()
        else:
            break

    if 'nickname' in request.session:
      try:
          player = Player.objects.get(nickname = request.session['nickname'])
      except:
          del request.session['nickname']

        
        
          
        
        
