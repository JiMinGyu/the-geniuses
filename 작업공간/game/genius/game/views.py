﻿# -*- coding: utf-8 -*-

from django.http import HttpResponse
from django.template import Context, loader
from django.views.decorators.csrf import csrf_exempt
from django.utils import timezone
from django.shortcuts import render, redirect

from game.models import Room, Player
from game.Indiangame import *

# Create your views here.
def index(request):

    
    #세션이 존재할 경우, 대기 룸 화면을 보여준다.
    if request.session.get('nickname',) != None:
        return redirect('/room')

    tpl = loader.get_template('index.html')
    ctx = Context({})
    
    return HttpResponse(tpl.render(ctx))

@csrf_exempt
def introduction(request):
    
    #세션이 존재하지 않을 경우, 세션을 설정하고 세션이 존재할 경우 게임 설정 화면을 출력한다. 
    if request.session.get('nickname',) == None:
        try:
            
            nickname = False
            nickname = request.POST['nickname']
            nickname = nickname.strip()

            #닉네임이 공백이었다면,
            if nickname == '':
                raise NameError

            new_player = Player(nickname = nickname)
            #Player의 nickname 필드는 유일(unique)하도록 설정했으므로 만약,
            #동일한 닉네임을 가진 플레이어가 데이터베이스에 존재한다면 예외가 발생.
            new_player.save()
            #닉네임 세션을 설정.
            request.session['nickname'] = nickname
                
        except NameError:
             return HttpResponse(
                    "<script>alert('닉네임을 입력하지 않았습니다. 다시 입력해주세요.'); history.go(-1);</script>");
        except:
             return HttpResponse(
                    "<script>alert('이미 존재하는 닉네임입니다. 다른 닉네임을 입력해주세요.'); history.go(-1);</script>");
        
    tpl = loader.get_template('game_introduction.html')
    ctx = Context({
    })
        
    return HttpResponse(tpl.render(ctx))

@csrf_exempt
def room_list(request):

    session_nickname = request.session.get('nickname',)

    if session_nickname == None:
        return redirect('/index')
    
    rooms = Room.objects.all()
    tpl = loader.get_template('room_list.html')
    ctx = Context({
    'rooms' : rooms
    })
		
    return HttpResponse(tpl.render(ctx))



@csrf_exempt
def room(request, name):

    session_nickname = request.session.get('nickname',)
    match_player = False
    game_set = False
    waiting = False
    already_joined = False
    
    if session_nickname == None:
        return redirect('/room')

    try:
        room = Room.objects.get(name = name)
        
    except:#방이 존재하지 않을 경우 방을 생성한다.
        new_room = Room(name = name)
        new_room.save()
        room = Room.objects.get(name = name)
    
    try:

        #세션이 존재할 경우, 해당 플레이어는 무조건 존재한다.
        player = Player.objects.get(nickname = session_nickname)
        is_entered = False

        #방에 있는 플레이어를 조사해 현재 요청한 세션의 닉네임을 가진 플레이어가 있는지 확인한다.
        #방에 있는 플레이어를 확인할 겸, 상대방 플레이어도 설정한다.
        for room_player in room.join_players.all():
            
            if room_player.nickname == session_nickname:
                is_entered = True
            else:
                match_player = room_player

        #방 상태가 대기중일 경우, 해당 방의 유저가 아니라면 방의 유저로 추가한다.
        if room.is_playing == False:

            #플레이어가 다른 방에 조인했다면,
            if player.is_joined and is_entered == False:
                return HttpResponse(
                    "<script>alert('이미 다른 방에 참여하고 있습니다.'); history.go(-1);</script>");
            else:#다른 방에 조인하지 않았다면,
                player.is_joined = True
            
            #게임 정원이 충족되었으므로, 초기화를 한다.
            if room.current_player_number == 2:
                Initialize(player)
                Initialize(match_player)
                SetTurn(player, match_player)
                match_player.save()
                room.stack = 0
                room.is_playing = True
            #room.current_player_number < 2 and is_entered == False
            elif is_entered == False:
                player.is_joined = True
                room.join_players.add(player)
                room.current_player_number += 1

        #방이 시작되었고 해당 방의 유저가 아닐 경우
        elif is_entered == False:
            return HttpResponse(
                "<script>alert('이미 시작된 방입니다. 다른 방을 이용해주세요.'); history.go(-1);</script>");
        
    except:
        return HttpResponse("데이터 베이스 관련 처리 도중, 문제가 발생하였습니다.")



    #게임이 시작되었다면
    if room.is_playing == True:

        #패배한 플레이어가 있는지 EndCheck를 통해 테스트.
        lose_player = Endcheck(player, match_player)
        
        if lose_player:#패배한 플레이어가 있다면,
            if request.session.get('nickname',) == lose_player.nickname:
                room.join_players.filter(nickname = lose_player.nickname).delete()
                room.is_playing = False
                room.current_player_number -= 1
                del request.session['nickname']
                room.save()
                return HttpResponse(
                "<script>alert('게임에서 패배하였습니다. 닉네임 설정창으로 이동합니다.'); location.replace('/index');</script>");
            waiting = True

        #게임에서 진 플레이어가 없다면, 해당 플레이어의 턴인지 판단한다.    
        elif player.is_my_turn == True:
            
            try:
                is_bet = Betinput(player, match_player, request.POST['bet'])
                if is_bet == 0:
                    #배팅액이 같을 경우 승패를 판단해 적용시킨다.
                    if player.bet == match_player.bet:
                        room.stack = Over(player, match_player, room.stack)
                        Matchstart(player, match_player)

                    #턴을 설정한다.
                    SetTurn(player, match_player)
                    match_player.save()
                elif is_bet == 1:
                    return HttpResponse(
                    "<script>alert('상대방이 배팅한 칩보다 많은 칩을 입력해야 합니다.'); history.go(-1);</script>");
                elif is_bet == 2:
                    return HttpResponse(
                    "<script>alert('사용 가능한 칩의 갯수를 초과했습니다.'); history.go(-1);</script>");
                elif is_bet == 3:
                    return HttpResponse(
                    "<script>alert('상대방의 칩의 갯수보다 적게 입력해야 합니다.'); history.go(-1);</script>");

                            
            except:
                if 'die' in request.POST:
                    SetTurn(player, match_player)
                    Lose(player, match_player, room.stack)
                    Matchstart(player, match_player)
                    room.stack = 0
                    match_player.save()
                        
    else:#room.is_playing == False
        waiting = True
 
    ctx = Context({
    'player' : player,
    'match_player' : match_player,
    'room' : room,
    })
    
    if waiting == False:#게임이 진행중이라면,
    
        if player.is_show_result != 0:
                        
            if player.is_show_result == 1:#게임에 이겼을 경우
                tpl = loader.get_template('win.html')
            elif player.is_show_result == 2:#게임에 졌을 경우
                tpl = loader.get_template('lose.html')
            else:#player.is_show_result == 3:#게임에 비겼을 경우
                tpl = loader.get_template('draw.html')
            player.is_show_result = 0

        else:
            tpl = loader.get_template('game.html')
                            
    elif waiting == True:#상대방을 기다리는 중이라면,
        if 'exit' in request.GET:
            player.is_joined = False
            player.save()
            room.delete()
            return redirect('/room')
        
        tpl = loader.get_template('waiting.html')
        ctx = Context({
        })

    #플레이어와 방의 정보를 저장한다.
    player.save()
    room.save()

    return HttpResponse(tpl.render(ctx))

def exit(request):

    #세션이 존재할 경우, 대기 룸 화면을 보여준다.
    if request.session.get('nickname',) != None:
        return redirect('/room')

    tpl = loader.get_template('index.html')
    ctx = Context({})
    
    return HttpResponse(tpl.render(ctx))
