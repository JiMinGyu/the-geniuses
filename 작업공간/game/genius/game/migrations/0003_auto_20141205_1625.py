# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('game', '0002_auto_20141205_1510'),
    ]

    operations = [
        migrations.AddField(
            model_name='player',
            name='prev_card',
            field=models.PositiveIntegerField(default=0),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='player',
            name='is_show_result',
            field=models.PositiveIntegerField(default=0),
            preserve_default=True,
        ),
    ]
