# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('game', '0003_auto_20141123_2059'),
    ]

    operations = [
        migrations.RenameField(
            model_name='room',
            old_name='join_player',
            new_name='join_players',
        ),
    ]
