# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('game', '0002_auto_20141123_2057'),
    ]

    operations = [
        migrations.AlterField(
            model_name='room',
            name='is_playing',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]
