import random

turn = 0;
stack = 0;
gcont = True;
def Firststart(A, B):
	A, B = Initialize(A, B);# 초기화

def Gamestart(A, B, turn, stack, gcont):
	if gcont == True: # 게임 한 세트가 종료 됐을 시 실행
		A,B=Matchstart(A,B); # 매치 시작, 기본으로 칩 1개 베팅
		A.card = Randomcard();
		B.card = Randomcard();
	if Endcheck(A, B) == 1: # 칩이 0인 유저 확인
		return A, B, turn, stack, gcont;
	Printscore(A,B,stack); # 현재 점수 출력
	Player, Ene = Turn(A, B, turn); # 턴 계산
	Cardopen(Ene); # 상대방의 카드 공개
	Printbet(A,B); # 칩 베팅 현황 출력
	cmd, turn, gcont = Betinput(Player, Ene, turn, gcont);
	# 베팅한 결과에 따라, cmd, turn 리턴
	if cmd == 'err': # 입력에 오류 있을 시 실행
		Err();
	elif cmd == 'lose': # 0개 배팅, 게임 종료
		Player, Ene, stack, gcont = Lose(Player, Ene, stack, gcont);
	elif cmd == 'over': # 상대와 동일하게 배팅
		Player, Ene, stack, gcont = Over(Player, Ene, stack, gcont);
	if cmd != 'err': # 
		print("★★★★★★★★★★★★★★★★★★★★");
		Printscore(A,B,stack); # 현재 점수 출력
		print("★★★★★★★★★★★★★★★★★★★★");
	return A, B, turn, stack, gcont;
		
class Playerdata:
   card = 0; # 현재 카드
   chip = 30; # 현재 보유 칩
   bet = 0; # 플레이어가 베팅에 건 칩
   name = 'A';

def Randomcard(): # 1~10 의 숫자중 난수 하나 리턴
      Cardarr = [1,2,3,4,5,6,7,8,9,10];
      return random.sample(Cardarr,1);

def Initialize(A, B): # 게임 처음 시작 시 초기화
	A.chip = 30;
	B.chip = 30;
	A.card = Randomcard();
	B.card = Randomcard();
	A.bet = 0;
	B.bet = 0;
	A.name = 'A';
	B.name = 'B';
	return A, B;

def Matchstart(A, B): # 매치 시작시 칩 1개 배팅
	A.chip -= 1;
	B.chip -= 1;
	A.bet += 1;
	B.bet += 1;
	return A, B;

def Endcheck(A, B): # 칩 0 인 유저 있을 시 게임 종료
	if A.chip <= 0:
		print("B플레이어 우승");
		return 1;
	elif B.chip <= 0:
		print("A플레이어 우승");
		return 1;
	else:
		return 0;

def Printscore(A, B, stack): # 현재 스코어 출력
	print("A의 칩의 갯수 :: ",A.chip);
	print("B의 칩의 갯수 :: ",B.chip);
	print("현재 배팅된 칩:: ",A.bet+B.bet);

def Turn(A, B, turn): # 차례를 계산하는 함수
	if turn%2 == 0:
		print("Player A turn");
		return A, B;
	elif turn%2 == 1:
		print("Player B turn");
		return B, A;

def Cardopen(Ene): # 상대방의 카드 보여주는 함수
	print("상대방의 카드 : ", Ene.card, "\n");

def Printbet(A, B): # 베팅 상황을 알려주는 함수
	print("A의 베팅 :: ", A.bet);
	print("B의 베팅 :: ", B.bet);

def Betinput(Player, Ene, turn, gcont): 
	while 1:
		print("베팅할 칩을 입력하세요");
		bet = input();
		if int(bet) == 0:
			print("베팅을 포기했습니다.",Ene.name,"플레이어 승");
			return 'lose', turn. gcont;
		elif Player.bet + int(bet) < Ene.bet:
			print("상대방보다 더 많이 입력 해야 합니다.");
			gcont = False;
			return 'err', turn, gcont;
		elif Player.bet + int(bet) > Ene.bet:
			if Player.chip < int(bet) :
				print("사용 가능한 칩의 갯수를 초과했습니다");
				return 'err', turn, gcont;
			elif int(bet)>Ene.chip:
				gcont = False;
				print("상대방의 칩의 갯수보다 적게 입력 해야 합니다.");
				return 'err', turn, gcont;
			else:
				Player.bet = Player.bet + int(bet);
				Player.chip = Player.chip - int(bet);
				print("베팅 완료, 상대방 턴");
				gcont = False;
				turn += 1;
				return 'betok', turn, gcont;
		elif Player.bet + int(bet) == Ene.bet:
			Player.bet += int(bet);
			Player.chip -= int(bet);
			print("베팅 종료");
			turn += 1;
			return 'over', turn, gcont;
		else:
			print("잘못된 값입니다.");
			return 'err', turn, gcont;

def Err():
	print("잘못된 입력입니다.");
	
def Lose(Player, Ene, stack, gcont):
	Ene.chip += Player.bet + Ene.bet + stack;
	Player.bet = 0;
	Ene.bet = 0;
	gcont = True;
	if Player.card == 10:
		print("플레이어의 카드가 10이었으므로 패널티");
		Player.chip -= 10;
		Ene.chip += 10;
	return Player, Ene, stack, gcont;

def Over(Player, Ene, stack, gcont):
	if Player.card == Ene.card: # 무승부
		stack = Player.bet + Ene.bet;
		Player.bet = 0;
		Ene.bet = 0;
		gcont = True;
		print("무승부가 되었습니다");
	elif Player.card > Ene. card :
		Player.chip += Player.bet + Ene.bet + stack;
		Player.bet = 0;
		Ene.bet = 0;
		stack = 0;
		gcont = True;
		print(Player.name,"님이 우승하셨습니다.");
	elif Player.card < Ene.card:
		Ene.chip += Player.bet + Ene.bet + stack;
		Player.bet = 0;
		Ene.bet = 0;
		stack = 0;
		gcont = True;
		print(Ene.name,"님이 우승하셨습니다.");
	return Player, Ene, stack, gcont;



//////////////////////////////////////////////////////

테스트 하는법

>>> A = Playerdata()
>>> B = Playerdata()
>>> Firststart(A, B)

까지 입력 한 뒤

>>> A, B, turn, stack, gcont = Gamestart(A, B, turn, stack, gcont);

을 입력하여 실행