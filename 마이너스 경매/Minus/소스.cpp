#include<iostream>
#include<cstdlib>
#include<ctime>
using namespace std;


void main(){

	int cube[33];
	srand((unsigned int)time(NULL));
	// -3 ~ -35 배열 생성
	for (int i = 0; i < 33; i++){
		cube[i] = -i - 3;
	}

	// -3 ~ -35 배열 중 히든큐브 선정
	int num = ((rand() % 32) + 3)*-1;
	for (int i = 0; i < 32; i++){
		if (cube[i] == num){
			cube[i] = 0;
		}
	}

	// 히든큐브를 33번째 배열로 뺌
	for (int i = 0; i < 33; i++){
		for (int j = 0; j < i; j++){
			if (cube[i] < cube[j]){
				num = cube[i];
				cube[i] = cube[j];
				cube[j] = num;
			}
		}
	}

	//셔플
	for (int i = 0; i < 32; i++){
		int x = rand() % 31;
		num = cube[i];
		cube[i] = cube[x];
		cube[x] = num;
	}

	//
	// ↑큐브 생성
	int Achip = 9, Bchip = 9; // 플레이어 A, B의 칩 갯수
	int An = 0, Bn = 0;  // A, B 플레이어의 큐브의 갯수를 나타낼 변수
	int Acube[32], Bcube[32]; // 소지한 큐브를 나타낼 배열
	for (int i = 0; i < 32; i++){ // 배열 초기화
		Acube[i] = 0;
		Bcube[i] = 0;
	}

	int Asum = 0, Bsum = 0; // 큐브의 숫자의 합을 나타낼 변수
	int Refuse = 0; // 거절 횟수를 나타낼 변수
	int turn = rand() % 2;
	//경매를 32번 진행
	for (int i = 0; i <32;){
		if (Refuse == 0){
			cout << "★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★\n";
			cout << i << "번째 큐브의 숫자는 " << cube[i++] << "입니다." << endl;
		}

		switch (turn % 2){  // 0 : 플레이어의 턴, 1 : 컴퓨터의 턴 / 낙찰 거부시 turn++;
		case 0: // A플레이어의 턴
			while (1){
				cout << "낙찰을 원하면 1, 원하지 않으면 0 을 눌러주세요 ";
				int num;
				cin >> num;

				// 낙찰받음
				if (Achip < Refuse){
					Asum += Acube[An++] = cube[i-1];
					Achip += Refuse; // 거절하는데 사용 된 칩 회수
					Refuse = 0; // 거부 횟수 초기화
					cout << "A플레이어가 거절할 칩이 부족하여 낙찰 받았습니다." << endl;
					break;
				}
				else if (num == 1){
					Asum += Acube[An++] = cube[i - 1];
					Achip += Refuse; // 거절하는데 사용 된 칩 회수
					Refuse = 0; // 거부 횟수 초기화
					cout << "A플레이어가 낙찰 받았습니다." << endl;
					break;
				}
				else if (num == 0){
					Achip--;  // 거절 했을 시 칩 감소
					Refuse++; // 거부 횟수 ++
					turn++; // 다음 사람으로 턴 이동
					cout << "A플레이어가 낙찰 거부했습니다." << endl;
					break;
				}
				else
					cout << "잘못된 숫자입니다. 다시 입력 해 주세요." << endl; // 입력이 잘못 됐을 시 다시 입력받음
			}
			break;
		case 1: // B플레이어의 턴(컴퓨터)
			while (1){
				int num;
				num = rand() % 2;
				// 낙찰받음
				if (Bchip < Refuse){
					Bsum += Bcube[Bn++] = cube[i - 1];
					Bchip += Refuse; // 거절하는데 사용 된 칩 회수
					Refuse = 0; // 거부 횟수 초기화
					cout << "B 플레이어가 거절할 칩이 부족하여 낙찰 받았습니다." << endl;
					break;
				}
				else if (num == 1){
					Bsum += Bcube[Bn++] = cube[i - 1];
					Bchip += Refuse; // 거절하는데 사용 된 칩 회수
					Refuse = 0; // 거부 횟수 초기화
					cout << "B플레이어가 낙찰 받았습니다." << endl;
					break;
				}
				else if (num == 0){
					Bchip--;  // 거절 했을 시 칩 감소
					Refuse++; // 거부 횟수 ++
					turn++; // 다음 사람으로 턴 이동
					cout << "B플레이어가 낙찰 거부했습니다." << endl;
					break;
				}
			}
			break;
		}
		if (Refuse == 0){
			cout << i << "번째 경매가 종료되었습니다." << endl << "현재 스코어" << endl;
			cout << "A플레이어 : ";
			int k = 0;
			while (Acube[k] != 0){ cout << Acube[k++] << " "; }
			cout << endl << "B플레이어 : ";
			k = 0;
			while (Bcube[k] != 0){ cout << Bcube[k++] << " "; }
			cout << endl;
		}
	}
	if (Asum + Achip < Bsum + Bchip)
		cout << "\nB플레이어 승!!";
	else if (Asum + Achip > Bsum + Bchip)
		cout << "\nA플레이어 승!!";
	else
		cout << "\n무승부!";
	cout << endl;
	
}